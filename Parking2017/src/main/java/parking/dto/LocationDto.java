package parking.dto;

import lombok.Data;

@Data
public class LocationDto {
    private int idLocation;
    private String address;
    private int numberSlots;
    private int numberOccupiedSlots;
    private int freeSlots;
    private double priceForHour;

    public LocationDto setAddress(String address) {
        this.address = address;
        return this;
    }

    public LocationDto setNumberSlots(int numberSlots) {
        this.numberSlots = numberSlots;
        return this;

    }

    public LocationDto setNumberOccupiedSlots(int numberOccupiedSlots) {
        this.numberOccupiedSlots = numberOccupiedSlots;
        return this;

    }

    public LocationDto setPriceForHour(double priceForHour) {
        this.priceForHour = priceForHour;
        return this;

    }

    public LocationDto setFreeSlots(int freeSlots) {
        this.freeSlots = freeSlots;
        return this;
    }
}
