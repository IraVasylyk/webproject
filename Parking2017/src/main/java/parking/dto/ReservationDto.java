package parking.dto;

import lombok.Data;
import parking.entity.Location;

import java.sql.Date;
import java.sql.Timestamp;

@Data
public class ReservationDto {


    private int idReservation;
    private Timestamp timeReserveOn;
    private Timestamp timeReserveUntil;
    private String address;
    private String model;
    private String number;
    private double price;
    private double priceForHour;

    public ReservationDto setIdReservation(int idReservation) {
        this.idReservation = idReservation;
        return this;
    }

    public ReservationDto setTimeReserveOn(Timestamp timeReserveOn) {
        this.timeReserveOn = timeReserveOn;
        return this;

    }

    public ReservationDto setTimeReserveUntil(Timestamp timeReserveUntil) {
        this.timeReserveUntil = timeReserveUntil;
        return this;

    }

    public ReservationDto setAddress(String address) {
        this.address = address;
        return this;

    }

    public ReservationDto setModel(String model) {
        this.model = model;
        return this;

    }

    public ReservationDto setNumber(String number) {
        this.number = number;
        return this;

    }

    public ReservationDto setPrice(double price) {
        this.price = price;
        return this;

    }

    public ReservationDto setPriceForHour(double priceForHour) {
        this.priceForHour = priceForHour;
        return this;

    }
}
