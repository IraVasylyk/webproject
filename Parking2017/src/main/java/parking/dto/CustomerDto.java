package parking.dto;

import lombok.Data;

@Data
public class CustomerDto {

    private String login;
    private String firstName;
    private String secondName;

    public CustomerDto setLogin(String login) {
        this.login = login;
        return this;
    }

    public CustomerDto setFirstName(String firstName) {
        this.firstName = firstName;
        return this;

    }

    public CustomerDto setSecondName(String secondName) {
        this.secondName = secondName;
        return this;

    }
}
