package parking.dto;

import lombok.Data;
import parking.validation.UniqueLogin;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class NewCustomerDto {
    @NotNull
    @UniqueLogin
    private String login;
    @NotNull
    @Size(min=6, max=20)
    private String password;


    @NotNull
    @Size(min=2, max=20)
    private String firstName;
    @NotNull
    @Size(min=2, max=20)
    private String secondName;
    @NotNull
    @Pattern(regexp="\\d{10}",message = "Must contains 10 digits.")
    private String phone;


}
