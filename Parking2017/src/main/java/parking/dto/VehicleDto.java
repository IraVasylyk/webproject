package parking.dto;

import lombok.Data;
@Data
public class VehicleDto {

  //  private int idVehicle;
    private String numberPlate;
    private String model;
    private boolean isParked;
    public Integer currentReservationCode;

//    public VehicleDto setIdVehicle(int idVehicle) {
//        this.idVehicle = idVehicle;
//        return this;
//    }

    public VehicleDto setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
        return this;
    }

    public VehicleDto setModel(String model) {
        this.model = model;
        return this;
    }

    public VehicleDto setParked(boolean parked) {
        isParked = parked;
        return this;
    }

    public VehicleDto setCurrentReservationCode(Integer code) {
        currentReservationCode = code;
        return this;
    }


}
