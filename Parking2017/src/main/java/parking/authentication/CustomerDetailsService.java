package parking.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import parking.dao.CustomerDao;
import parking.entity.Customer;
import parking.service.CustomerService;

import java.util.HashSet;
import java.util.Optional;

@Service
public class CustomerDetailsService implements UserDetailsService {

    @Autowired
    CustomerDao customerDao;
    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Optional<Customer> customerOpt=customerDao.getCustomer(login);





        if (!customerOpt.isPresent()) {

            throw new UsernameNotFoundException("User " + login + " was not found.");

        }


Customer customer=customerOpt.get();
        boolean accountNonExpired = true;

        boolean credentialsNonExpired = true;

        boolean accountNonLocked = true;



        return new org.springframework.security.core.userdetails.User(

                customer.getLogin(),customer.getPassword(),

               true,

                true,

                true,

                true,
                new HashSet<GrantedAuthority>());


    }
}
