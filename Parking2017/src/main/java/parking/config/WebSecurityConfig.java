package parking.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.transaction.annotation.Transactional;
import parking.authentication.CustomerDetailsService;

@Configuration

@EnableWebSecurity

@Transactional
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    CustomerDetailsService customerDetailsService;
    @Autowired

    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {



        // Users in memory. uncomment to try

        /*auth.inMemoryAuthentication().withUser("user1").password("12345").roles("USER");

        auth.inMemoryAuthentication().withUser("admin1").password("12345").roles("USER, ADMIN");*/



        // For User in database.

        auth.userDetailsService(customerDetailsService);



    }



    @Override

    protected void configure(HttpSecurity http) throws Exception {



        http.authorizeRequests()
                .antMatchers("/service","/rest/vehicle/**","/rest/*/owners**").authenticated()
                .anyRequest().permitAll()
                .and()
                .csrf().disable();


        // Config for Login Form

        http.authorizeRequests().and().formLogin()//

                // Submit URL of login page.

                .loginProcessingUrl("/j_spring_security_check") // Submit URL

                .loginPage("/login")//

                .defaultSuccessUrl("/")//
                .failureUrl("/login?error=true")//


                .usernameParameter("username")//

                .passwordParameter("password")



                .and().logout().logoutUrl("/logout").logoutSuccessUrl("/");



    }

}
