package parking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import parking.dto.ReservationDto;
import parking.service.ReservationService;

import static java.lang.Integer.valueOf;

@RestController
public class ReservationController {

    @Autowired
    ReservationService reservationService;


    @RequestMapping(value = "/rest/vehicle/park", method = RequestMethod.POST)
    public int parkVehicle(@RequestBody ReservationDto reservationDto) {
       return reservationService.parkVehicle(reservationDto);
    }

    @RequestMapping(value = "/rest/vehicle/pickUp", method = RequestMethod.POST)
    public void pickUpVehicle(@RequestParam("id_reservation") String id_reservation) {
        User user= (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        reservationService.pickUpVehicle(valueOf(id_reservation), user.getUsername());
    }

    @RequestMapping(value = "/rest/reservation/price", method = RequestMethod.GET)
    public ReservationDto getReceipt(@RequestParam("id_reservation") String id_reservation) {
        return reservationService.getReceipt(valueOf(id_reservation));
    }
}
