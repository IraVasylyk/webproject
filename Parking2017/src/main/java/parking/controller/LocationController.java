package parking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import parking.dto.LocationDto;
import parking.entity.Location;
import parking.service.LocationService;

import java.util.List;

@Controller
public class LocationController {

    @Autowired
    LocationService locationService;

    @RequestMapping(value = "/rest/locations",method = RequestMethod.GET)
    @ResponseBody
    public List<LocationDto> getLocations(){
        return locationService.getAllLocations();
    }
}
