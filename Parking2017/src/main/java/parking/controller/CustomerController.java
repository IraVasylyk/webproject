package parking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import parking.dto.NewCustomerDto;
import parking.dto.VehicleDto;
import parking.entity.Customer;
import parking.entity.Vehicle;
import parking.exception.ValidationException;
import parking.service.CustomerService;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CustomerController {

    @Autowired
    CustomerService customerService;



    @RequestMapping(value = "/rest/customer/vehicles", method = RequestMethod.GET)
    @ResponseBody
        public List<VehicleDto> getVehicles( ) {
        User user= (org.springframework.security.core.userdetails.User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            return customerService.getVehicleByCustomer(user.getUsername());
    }

    @RequestMapping(value = "/rest/customer/vehicle/add", method = RequestMethod.POST)
    @ResponseBody
    public void addVehicle( @RequestBody Vehicle vehicle) {
        User user= (org.springframework.security.core.userdetails.User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        customerService.addVehicleToCustomer(user.getUsername(),vehicle);
    }



    @RequestMapping(value = "/rest/customer/signUp", method = RequestMethod.POST)
    @ResponseBody
    public void registerCustomer(@RequestBody  @Valid NewCustomerDto customer, BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            throw new ValidationException(bindingResult.getFieldErrors());
        }
       customerService.saveCustomer(customer);
    }



}
