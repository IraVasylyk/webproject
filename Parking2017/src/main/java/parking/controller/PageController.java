package parking.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PageController {

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String welcome(){
        return "service";
    }

//    @RequestMapping(value = "/locations",method = RequestMethod.GET)
//    public String locations(){
//        return "locations";
//    }
//
//
//    @RequestMapping(value = "/service",method = RequestMethod.GET)
//    public String reservations(){
//        return "service";
//    }

//    @RequestMapping(value = "/beClient",method = RequestMethod.GET)
//    public String registrationCustomer(){
//        return "registrationCustomer";
//    }

    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public String login(){
        return "login";
    }


}
