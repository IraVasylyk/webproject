package parking.controller;

import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import parking.exception.ParkingException;
import parking.exception.ValidationException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(ParkingException.class)
    @ResponseBody
    public ResponseEntity<Error> handleUserError(HttpServletRequest req, ParkingException ex) {
        Error error = new Error();
        error.setMessage(ex.getMessage());
        error.setUrl(req.getRequestURL());
        return new ResponseEntity<>(error, ex.getResponseStatus());
    }
    @Data
    private class Error {
        private String message;
        private StringBuffer url;
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<List<FieldError>> handleValidationError(HttpServletRequest req, ValidationException ex) {

        return new ResponseEntity<>(ex.getFieldErrorList(), ex.getResponseStatus());
    }
}
