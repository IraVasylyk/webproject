package parking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import parking.dto.CustomerDto;
import parking.service.VehicleService;

import java.util.List;

@Controller
public class VehicleController {

    @Autowired
    VehicleService vehicleService;

    @RequestMapping(value = "/rest/vehicle/owners/possible", method = RequestMethod.GET)
    @ResponseBody
    public List<CustomerDto> getPossibleOwners(@RequestParam("numberPlate") String numberPlate) {
        return vehicleService.getPossibleNewCustomer(numberPlate);
    }

    @RequestMapping(value = "/rest/vehicle/owners", method = RequestMethod.GET)
    @ResponseBody
    public List<CustomerDto> getOwners(@RequestParam("numberPlate") String numberPlate) {
        return vehicleService.getCustomers(numberPlate);
    }


    @RequestMapping(value = "/rest/{numberPlate}/owners/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public void addOwners(@RequestBody List<String> newOwners, @PathVariable("numberPlate") String vehicleNumber) {
        vehicleService.addOwners(newOwners, vehicleNumber);
    }
}
