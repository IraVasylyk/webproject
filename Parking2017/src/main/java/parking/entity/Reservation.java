package parking.entity;

import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "reservation")
public class Reservation {

    @Id
    @GeneratedValue
    private int idReservation;

    @Column(nullable = false)
    private Timestamp timeReserveOn;
    private Timestamp timeReserveUntil;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_location")
    private Location location;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "id_vehicle")
    private Vehicle vehicle;

    public Reservation setIdReservation(int idReservation) {
        this.idReservation = idReservation;
        return this;

    }

    public Reservation setTimeReserveOn(Timestamp timeReserveOn) {
        this.timeReserveOn = timeReserveOn;
        return this;

    }

    public Reservation setTimeReserveUntil(Timestamp timeReserveUntil) {
        this.timeReserveUntil = timeReserveUntil;
        return this;

    }

    public Reservation setLocation(Location location) {
        this.location = location;
        return this;

    }

    public Reservation setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
        return this;
    }
}
