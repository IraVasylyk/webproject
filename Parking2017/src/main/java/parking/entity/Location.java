package parking.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Data
@ToString(exclude = {"reservation"})
@Entity
@Table(name="location")
public class Location {
    @Id
    @Column  (name = "id_location")
    private int idLocation;
    private String address;
    private int numberSlots;
    private int numberOccupiedSlots;
    private double priceForHour;

    @OneToMany(mappedBy = "location")
    private List<Reservation> reservations;
}
