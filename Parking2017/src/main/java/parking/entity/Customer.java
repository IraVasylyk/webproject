package parking.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;
import parking.validation.UniqueLogin;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@ToString(exclude = {"vehicles"})
@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @Column(name = "id_customer")
    @GeneratedValue
    private int idCustomer;

    @Column(unique = true)
    private String login;

    private String password;

    private String firstName;

    private String secondName;

    private String phone;

    @ManyToMany(mappedBy = "customers", fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private List<Vehicle> vehicles;


    public Customer setPassword(String password) {
        this.password = password;
        return this;

    }

    public Customer setPhone(String phone) {
        this.phone = phone;
        return this;

    }


    public Customer setLogin(String login) {
        this.login = login;
        return this;
    }

    public Customer setFirstName(String firstName) {
        this.firstName = firstName;
        return this;

    }

    public Customer setSecondName(String secondName) {
        this.secondName = secondName;
        return this;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Customer customer = (Customer) o;

        if (idCustomer != customer.idCustomer) return false;
        if (!login.equals(customer.login)) return false;
        if (password != null ? !password.equals(customer.password) : customer.password != null) return false;
        if (firstName != null ? !firstName.equals(customer.firstName) : customer.firstName != null) return false;
        if (secondName != null ? !secondName.equals(customer.secondName) : customer.secondName != null) return false;
        return phone != null ? phone.equals(customer.phone) : customer.phone == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + idCustomer;
        result = 31 * result + login.hashCode();
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        return result;
    }
}