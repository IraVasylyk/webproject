package parking.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Data
@ToString(exclude = {"reservations","customers"})
@Entity
@Table(name = "vechile")
public class Vehicle {

    @Id
    @Column(name="id_vehicle")
    @GeneratedValue
    private int idVehicle;

    @Column(nullable = false,unique = true)
    private String numberPlate;
    private String model;

    @Column(nullable = false)
    private boolean isParked;


    @JsonIgnore
    @OneToMany(mappedBy = "vehicle")
    private List<Reservation> reservations;


    @ManyToMany
    @JoinTable(name="vehicle_customer")
    private List<Customer> customers;


}