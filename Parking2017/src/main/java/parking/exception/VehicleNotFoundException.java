package parking.exception;

public class VehicleNotFoundException extends ParkingNotFoundException {
    private String message;

    public VehicleNotFoundException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }


    public VehicleNotFoundException() {
        message = "VehicleNotFound";
    }
}
