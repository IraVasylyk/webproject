package parking.exception;

import org.springframework.http.HttpStatus;

public class LocationIsFullyOccupiedException extends ParkingException {
    public HttpStatus getResponseStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    private String message;

    public LocationIsFullyOccupiedException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }


    public LocationIsFullyOccupiedException() {
        message = "LocationIsFullyOccupied";
    }
}
