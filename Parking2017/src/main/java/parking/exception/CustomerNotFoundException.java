package parking.exception;

public class CustomerNotFoundException extends ParkingNotFoundException {
    private String message;

    public CustomerNotFoundException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }


    public CustomerNotFoundException() {
        message = "CustomerNotFound";
    }
}
