package parking.exception;

import org.springframework.http.HttpStatus;

public class AccessDeniedException extends ParkingException {
    @Override
    public HttpStatus getResponseStatus() {
        return HttpStatus.UNAUTHORIZED;
    }

    private String message;

    public AccessDeniedException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }


    public AccessDeniedException() {
        message = "AccessDenied";
    }
}
