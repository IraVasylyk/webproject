package parking.exception;

public class LocationNotFoundException extends ParkingNotFoundException {
    private String message;

    public LocationNotFoundException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public LocationNotFoundException() {
        message = "LocationNotFound";
    }
}
