package parking.exception;

public class ReservationNotFoundException extends ParkingNotFoundException {
    private String message;

    public ReservationNotFoundException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }


    public ReservationNotFoundException() {
        message = "ReservationNotFound";
    }
}
