package parking.exception;

import org.springframework.http.HttpStatus;

public class VehicleAlreadyPickedUpException extends ParkingException {

    @Override
    public HttpStatus getResponseStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    private String message;

    public VehicleAlreadyPickedUpException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }


    public VehicleAlreadyPickedUpException() {
        message = "VehicleAlreadyPickedUp";
    }
}
