package parking.exception;

import org.springframework.http.HttpStatus;

public abstract class ParkingNotFoundException extends ParkingException {
    public HttpStatus getResponseStatus() {
        return HttpStatus.NOT_FOUND;
    }


}
