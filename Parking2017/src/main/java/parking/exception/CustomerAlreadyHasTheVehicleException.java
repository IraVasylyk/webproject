package parking.exception;

import org.springframework.http.HttpStatus;

public class CustomerAlreadyHasTheVehicleException extends ParkingException {
    @Override
    public HttpStatus getResponseStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    private String message;

    public CustomerAlreadyHasTheVehicleException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }


    public CustomerAlreadyHasTheVehicleException() {
        message = "CustomerAlreadyHasTheVehicle";
    }
}
