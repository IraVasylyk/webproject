package parking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import java.util.List;

public class ValidationException extends RuntimeException {
    private List<FieldError> fieldErrorList;

    public ValidationException(List<FieldError> fieldErrorList) {
        this.fieldErrorList = fieldErrorList;
    }

    public List<FieldError> getFieldErrorList() {
        return fieldErrorList;
    }

    public HttpStatus getResponseStatus() {
        return HttpStatus.BAD_REQUEST;
    }
}
