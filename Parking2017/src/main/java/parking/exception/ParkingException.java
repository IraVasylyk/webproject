package parking.exception;

import org.springframework.http.HttpStatus;

public abstract class ParkingException extends RuntimeException {
    public abstract HttpStatus getResponseStatus();
}
