package parking.exception;

import org.springframework.http.HttpStatus;

public class VehicleIsAlreadyParkedException extends ParkingException {
    public HttpStatus getResponseStatus(){
        return HttpStatus.BAD_REQUEST;
    }

    private String message;

    public VehicleIsAlreadyParkedException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }



    public VehicleIsAlreadyParkedException() {
        message = "VehicleIsAlreadyParked";
    }
}
