package parking.service;

import parking.dto.LocationDto;
import parking.entity.Location;

import java.util.List;

public interface LocationService {
    List<LocationDto> getAllLocations();
    int getNumberFreeSlot(String address) ;
    Location getByAddress(String address);
    void parkVehicle(String address);
    void pickUpVehicle(Location location);
}
