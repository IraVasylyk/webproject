package parking.service;

import parking.dto.CustomerDto;
import parking.entity.Customer;
import parking.entity.Vehicle;
import parking.exception.VehicleIsAlreadyParkedException;
import parking.exception.VehicleNotFoundException;

import java.util.List;

public interface VehicleService {

    void parkVehicle(String number);


    Vehicle getByNumber(String number);

    void pickUpVehicle(Vehicle vehicle);

    void addVehicle(Vehicle vehicle);

    Integer getCurrentReservationCode(String number);

    List<CustomerDto> getPossibleNewCustomer(String number);

    List<CustomerDto> getCustomers(String number);

    void addOwners(List<String> customerLogin, String number);
}
