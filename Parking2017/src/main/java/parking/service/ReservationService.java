package parking.service;

import parking.dto.ReservationDto;
import parking.exception.*;

public interface ReservationService {

    int parkVehicle(ReservationDto reservation) throws LocationIsFullyOccupiedException, LocationNotFoundException, VehicleNotFoundException, VehicleIsAlreadyParkedException;

    ReservationDto getReceipt(int idReservation) throws ReservationNotFoundException;

    void pickUpVehicle(int idReservation,String login) throws ReservationNotFoundException ;
}
