package parking.service;

import parking.dto.NewCustomerDto;
import parking.dto.VehicleDto;
import parking.entity.Customer;
import parking.entity.Vehicle;

import java.util.List;

public interface CustomerService {

    List<Customer> getAllCustomerBesidesTheIndicated(String login);

    void addVehicleToCustomer(String login, Vehicle vehicle);

    List<VehicleDto> getVehicleByCustomer(String login);

    Customer getByLogin(String login);

    void saveCustomer(NewCustomerDto customer);

    boolean isCustomerExist(String login);
}
