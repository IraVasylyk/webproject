package parking.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import parking.dao.LocationDao;
import parking.dto.LocationDto;
import parking.entity.Location;
import parking.exception.LocationIsFullyOccupiedException;
import parking.exception.LocationNotFoundException;
import parking.service.LocationService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class LocationServiceImpl implements LocationService {
    @Autowired
    LocationDao locationDao;

    @Override
    public List<LocationDto> getAllLocations() {
        return locationDao.getAllLocation()
                .stream()
                .map(this::mapToDto).collect(Collectors.toList());
    }

    @Override
    public int getNumberFreeSlot(String address)  {
        Location location=getByAddress(address);
        return location.getNumberSlots()-location.getNumberOccupiedSlots();
    }

    @Override
    public Location getByAddress(String address)  {
        return locationDao.getLocationByAddress(address).orElseThrow(LocationNotFoundException::new);
    }

    @Override
    public void parkVehicle(String address) {

        Location location=getByAddress(address);
        if(location.getNumberSlots()>location.getNumberOccupiedSlots()) {
            location.setNumberOccupiedSlots(location.getNumberOccupiedSlots() + 1);
            locationDao.updateLocation(location);
        }else{
            throw new LocationIsFullyOccupiedException();
        }

    }

    @Override
    public void pickUpVehicle(Location location) {
        location.setNumberOccupiedSlots(location.getNumberOccupiedSlots() - 1);
        locationDao.updateLocation(location);

    }

    private LocationDto mapToDto(Location location){
        return new LocationDto()
                .setAddress(location.getAddress())
                .setNumberOccupiedSlots(location.getNumberOccupiedSlots())
                .setNumberSlots(location.getNumberSlots())
                .setFreeSlots(location.getNumberSlots()-location.getNumberOccupiedSlots())
                .setPriceForHour(location.getPriceForHour());

    }
}
