package parking.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import parking.dao.ReservationDao;
import parking.dto.ReservationDto;
import parking.entity.Customer;
import parking.entity.Reservation;
import parking.exception.*;
import parking.service.CustomerService;
import parking.service.LocationService;
import parking.service.ReservationService;
import parking.service.VehicleService;


import java.sql.Timestamp;

@Service
@Transactional
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ReservationDao reservationDao;

    @Autowired
    private LocationService locationService;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private CustomerService customerService;

    @Override
    public int parkVehicle(ReservationDto reservation) {
        locationService.parkVehicle(reservation.getAddress());
        vehicleService.parkVehicle(reservation.getNumber());
        reservation.setTimeReserveOn(new Timestamp(System.currentTimeMillis()));
     return   reservationDao.insertReservation(mapToEntity(reservation));
    }

    @Override
    public ReservationDto getReceipt(int idReservation) {
        Reservation reservation=reservationDao.findById(idReservation).orElseThrow(ReservationNotFoundException::new);
        double hourAmount=(System.currentTimeMillis()-reservation.getTimeReserveOn().getTime())/3600000+1.0;
        return mapToDto(reservation).setPrice(hourAmount*reservation.getLocation().getPriceForHour());


    }

    @Override
    public void pickUpVehicle(int idReservation,String login)  {
        Reservation reservation=reservationDao.findById(idReservation).orElseThrow(ReservationNotFoundException::new);
        Customer customer = customerService.getByLogin(login);
        if(!customer.getVehicles().contains(reservation.getVehicle())){
            throw new AccessDeniedException();
        }
        if(reservation.getTimeReserveUntil()!=null){
            throw new VehicleAlreadyPickedUpException();
        }
        locationService.pickUpVehicle(reservation.getLocation());
        vehicleService.pickUpVehicle(reservation.getVehicle());
        reservation.setTimeReserveUntil(new Timestamp(System.currentTimeMillis()));
        reservationDao.updateReservation(reservation);
    }

    private Reservation mapToEntity(ReservationDto reservationDto)  {
        return new Reservation()
                .setLocation(locationService.getByAddress(reservationDto.getAddress()))
                .setTimeReserveOn(reservationDto.getTimeReserveOn())
                .setVehicle(vehicleService.getByNumber(reservationDto.getNumber()));
    }

    private ReservationDto mapToDto(Reservation reservation)  {
        return new ReservationDto()
                .setIdReservation(reservation.getIdReservation())
                .setAddress(reservation.getLocation().getAddress())
                .setPriceForHour(reservation.getLocation().getPriceForHour())
                .setTimeReserveOn(reservation.getTimeReserveOn())
                .setModel(reservation.getVehicle().getModel())
                .setNumber(reservation.getVehicle().getNumberPlate())
                .setTimeReserveUntil(new Timestamp(System.currentTimeMillis()));
    }



}
