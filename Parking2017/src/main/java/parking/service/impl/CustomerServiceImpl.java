package parking.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import parking.dao.CustomerDao;
import parking.dao.ReservationDao;
import parking.dao.VehicleDao;
import parking.dto.NewCustomerDto;
import parking.dto.VehicleDto;
import parking.entity.Customer;
import parking.entity.Vehicle;
import parking.exception.CustomerAlreadyHasTheVehicleException;
import parking.exception.CustomerNotFoundException;
import parking.service.CustomerService;
import parking.service.VehicleService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    CustomerDao customerDao;
    @Autowired
    VehicleDao vehicleDao;

    @Autowired
   VehicleService vehicleService;

    @Override
    public List<Customer> getAllCustomerBesidesTheIndicated(String login) {
        Customer customer = customerDao.getCustomer(login).orElseThrow(CustomerNotFoundException::new);
        List<Customer> allCustomers = customerDao.getAllCustomer();
        allCustomers.remove(customer);
        return allCustomers;
    }

    @Override
    public void addVehicleToCustomer(String login, Vehicle vehicle) {
        Customer customer = customerDao.getCustomer(login).orElseThrow(CustomerNotFoundException::new);
        Optional<Vehicle> vehicleOpt=vehicleDao.findByNumber(vehicle.getNumberPlate());
        if (vehicleOpt.isPresent()) {
           vehicle=vehicleOpt.get();
        }else {
            vehicleDao.addVehicle(vehicle);
        }
        List<Vehicle> vehicles = customer.getVehicles();
        if(vehicles.contains(vehicle)){
            throw new CustomerAlreadyHasTheVehicleException();
        }
        vehicles.add(vehicle);

        customer.setVehicles(vehicles);
        customerDao.updateCustomer(customer);

        List<Customer> customers=vehicle.getCustomers();
        if(customers==null){
            customers=new ArrayList<>();
        }
        customers.add(customer);
        vehicle.setCustomers(customers);
        vehicleDao.updateVehicle(vehicle);
    }

    @Override
    public List<VehicleDto> getVehicleByCustomer(String login) {
        Customer customer = customerDao.getCustomer(login).orElseThrow(CustomerNotFoundException::new);
        return customer.getVehicles().stream().map(this::mapVehicleToDto).collect(Collectors.toList());
    }

    @Override
    public Customer getByLogin(String login){
       return customerDao.getCustomer(login).orElseThrow(CustomerNotFoundException::new);
    }

    @Override
    public void saveCustomer(NewCustomerDto customer){

        customerDao.addCustomer(mapDtoToCustomer(customer));
    }


    @Override
    public boolean isCustomerExist(String login){
        return customerDao.getCustomer(login).isPresent();
    }

    private VehicleDto mapVehicleToDto(Vehicle vehicle){
        return new VehicleDto()
               // .setIdVehicle(vehicle.getIdVehicle())
                .setModel(vehicle.getModel())
                .setNumberPlate(vehicle.getNumberPlate())
                .setParked(vehicle.isParked())
                .setCurrentReservationCode(vehicleService.getCurrentReservationCode(vehicle.getNumberPlate()));
    }

    private Customer mapDtoToCustomer(NewCustomerDto customerDto){
        return new Customer().setLogin(customerDto.getLogin()).setPassword(customerDto.getPassword()).setFirstName(customerDto.getFirstName())
                .setSecondName(customerDto.getSecondName()).setPhone(customerDto.getPhone());
    }
}
