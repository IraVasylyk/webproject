package parking.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import parking.dao.CustomerDao;
import parking.dao.ReservationDao;
import parking.dao.VehicleDao;
import parking.dto.CustomerDto;
import parking.entity.Customer;
import parking.entity.Reservation;
import parking.entity.Vehicle;
import parking.exception.VehicleAlreadyPickedUpException;
import parking.exception.VehicleIsAlreadyParkedException;
import parking.exception.VehicleNotFoundException;
import parking.service.CustomerService;
import parking.service.VehicleService;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class VehicleServiceImpl implements VehicleService {

    @Autowired
    VehicleDao vehicleDao;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    CustomerService customerService;
    @Autowired
    ReservationDao reservationDao;

    @Override
    public void parkVehicle(String number) {
        Vehicle vehicle = getByNumber(number);
        if (vehicle.isParked()) {
            throw new VehicleIsAlreadyParkedException();
        }
        vehicle.setParked(true);
        vehicleDao.updateVehicle(vehicle);
    }



    @Override
    public Vehicle getByNumber(String number) {
        return vehicleDao.findByNumber(number).orElseThrow(VehicleNotFoundException::new);
    }

    @Override
    public void pickUpVehicle(Vehicle vehicle) {
        vehicle.setParked(false);
        vehicleDao.updateVehicle(vehicle);
    }

    @Override
    public void addVehicle(Vehicle vehicle) {
        vehicleDao.addVehicle(vehicle);
    }

    @Override
    public Integer getCurrentReservationCode(String number) {
        Vehicle vehicle = getByNumber(number);

        Optional<Reservation> reservation = reservationDao.activeReservation().stream().filter(res -> res.getVehicle().equals(vehicle)).findFirst();

       if(reservation.isPresent()){
           return reservation.get().getIdReservation();
       }
       return null;

    }

    @Override
    public List<CustomerDto> getPossibleNewCustomer(String number) {
        List<Customer> allCustomers=customerDao.getAllCustomer();
        Vehicle vehicle = getByNumber(number);
        return allCustomers
                .stream()
                .filter(customer -> !vehicle.getCustomers().contains(customer))
                .map(this::mapToCustomerDto)
                .sorted(Comparator.comparing(CustomerDto::getLogin))
                .collect(Collectors.toList());
    }

    @Override
    public List<CustomerDto> getCustomers(String number) {

        Vehicle vehicle = getByNumber(number);
        return vehicle.getCustomers()
                .stream()
                .map(this::mapToCustomerDto)
                .sorted(Comparator.comparing(CustomerDto::getLogin))
                .collect(Collectors.toList());
    }

    private CustomerDto mapToCustomerDto(Customer customer){
        return new CustomerDto().setFirstName(customer.getFirstName()).setLogin(customer.getLogin()).setSecondName(customer.getSecondName());
    }

    @Override
    public void addOwners(List<String> customerLogin,String number) {
        Vehicle vehicle=getByNumber(number);
        for (String login:customerLogin) {
            customerService.addVehicleToCustomer(login,vehicle);
        }
    }
}
