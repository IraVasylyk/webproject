package parking.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UniqueLoginValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueLogin {

    String message() default "Customer with this login has already exist. Type another login.";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
