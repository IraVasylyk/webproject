package parking.validation;

import org.springframework.beans.factory.annotation.Autowired;
import parking.service.CustomerService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class UniqueLoginValidator implements ConstraintValidator<UniqueLogin,String> {

    @Autowired
    CustomerService customerService;

    @Override
    public void initialize(UniqueLogin uniqueLogin) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return !customerService.isCustomerExist(s);
    }
}
