package parking.dao;


import parking.entity.Customer;
import parking.entity.Vehicle;

import java.util.List;
import java.util.Optional;

public interface VehicleDao {

    Optional<Vehicle> findByNumber(String number);
    void updateVehicle(Vehicle vehicle);
    void addVehicle(Vehicle vehicle);


}
