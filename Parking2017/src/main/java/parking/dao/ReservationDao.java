package parking.dao;

import parking.entity.Reservation;

import java.util.List;
import java.util.Optional;


public interface ReservationDao {

    int insertReservation(Reservation reservation);

    Optional<Reservation> findById(int id);

    void updateReservation(Reservation reservation);

    List<Reservation> activeReservation();
}
