package parking.dao;

import parking.entity.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerDao {

    List<Customer> getAllCustomer();
    Optional<Customer> getCustomer(String login);
    void updateCustomer(Customer customer);
    void addCustomer(Customer customer);


}
