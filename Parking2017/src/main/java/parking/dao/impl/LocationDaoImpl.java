package parking.dao.impl;

import org.springframework.stereotype.Repository;
import parking.dao.LocationDao;
import parking.entity.Location;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Repository
public class LocationDaoImpl implements LocationDao {

@PersistenceContext
    EntityManager em;

    @Override
    public List<Location> getAllLocation() {
        return em.createQuery("Select l from Location l",Location.class).getResultList();
    }

    @Override
    public Optional<Location> getLocationByAddress(String address) {
        String queryWithNamedParams = "select i from Location i where i.address = :address";
        final TypedQuery<Location> query = em.createQuery(queryWithNamedParams, Location.class);

        query.setParameter("address", address);
        try {
            return Optional.ofNullable(query.getSingleResult());
        }catch (NoResultException e){
            return Optional.empty();
        }

    }

    @Override
    public void updateLocation(Location location) {
        em.merge(location);
    }
}
