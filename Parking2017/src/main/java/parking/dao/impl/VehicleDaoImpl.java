package parking.dao.impl;

import org.springframework.stereotype.Repository;
import parking.dao.VehicleDao;
import parking.entity.Vehicle;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Optional;

@Repository
public class VehicleDaoImpl implements VehicleDao {

    @PersistenceContext
    EntityManager em;
    @Override
    public Optional<Vehicle> findByNumber(String number) {
        String queryWithNamedParams = "select i from Vehicle i where i.numberPlate = :numberPlate";
        final TypedQuery<Vehicle> query = em.createQuery(queryWithNamedParams, Vehicle.class);

        query.setParameter("numberPlate", number);
        try {
            return Optional.ofNullable(query.getSingleResult());
        }catch (NoResultException e){
            return Optional.empty();
        }
    }
    @Override
    public void updateVehicle(Vehicle vehicle){
        em.merge(vehicle);
    }

    @Override
    public void addVehicle(Vehicle vehicle){
        em.persist(vehicle);
    }


}
