package parking.dao.impl;

import org.springframework.stereotype.Repository;
import parking.dao.CustomerDao;
import parking.entity.Customer;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Repository
public class CustomerDaoImpl implements CustomerDao {
    @PersistenceContext
    EntityManager em;

    @Override
    public List<Customer> getAllCustomer() {
        return em.createQuery("Select c from Customer c",Customer.class).getResultList();
    }

    @Override
    public Optional<Customer> getCustomer(String login) {
        String queryWithNamedParams = "select i from Customer i where i.login = :login";
        final TypedQuery<Customer> query = em.createQuery(queryWithNamedParams, Customer.class);

        query.setParameter("login", login);
        try {
            return Optional.ofNullable(query.getSingleResult());
        }catch (NoResultException e){
            return Optional.empty();
        }
    }

   public void updateCustomer(Customer customer){
        em.merge(customer);
   }

   public void addCustomer(Customer customer){
       em.persist(customer);
   }

}
