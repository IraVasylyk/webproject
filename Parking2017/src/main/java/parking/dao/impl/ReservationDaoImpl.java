package parking.dao.impl;

import org.springframework.stereotype.Repository;
import parking.dao.ReservationDao;
import parking.entity.Reservation;
import parking.entity.Vehicle;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class ReservationDaoImpl implements ReservationDao {
    @PersistenceContext
    EntityManager em;

    @Override
    public int insertReservation(Reservation reservation) {

        em.persist(reservation);
        em.flush();
        return reservation.getIdReservation();
    }

    @Override
    public Optional<Reservation> findById(int id) {
        return Optional.ofNullable(em.find(Reservation.class, id, LockModeType.NONE));
    }

    @Override
    public void updateReservation(Reservation reservation) {
        em.merge(reservation);
    }

    @Override
    public List<Reservation> activeReservation() {
        String queryWithNamedParams = "select i from Reservation i where i.timeReserveUntil is NULL";
        final TypedQuery<Reservation> query = em.createQuery(queryWithNamedParams, Reservation.class);


        try {
            return query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }

}
