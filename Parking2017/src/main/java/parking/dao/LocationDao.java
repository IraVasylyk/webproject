package parking.dao;

import parking.entity.Location;

import java.util.List;
import java.util.Optional;

public interface LocationDao {

    List<Location> getAllLocation();
    Optional<Location> getLocationByAddress(String address);
    void updateLocation(Location location);

}
