<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>

    <title>Parking</title>
    <jsp:include page="metadata.jsp"/>

</head>
<body>
<jsp:include page="header.jsp"/>
</br>
</br>
<div class="container">
    <img class="welcome-image" src="<c:url value="/resources/img/parkingGreen.png" />">
    <div id="benefits">
        <h3>Why you should be our client?</h3>
        <ul style="list-style-type:none">
            <li><span class="glyphicon glyphicon-ok"></span><b>Taked care of the camera</b>
                <p>Your car is equipped with a VIP-park 24 hours a day, 7 days a week with camera monitoring.</p>
            </li>
            <li><span class="glyphicon glyphicon-ok"></span><b>Covered parking lot</b>
                <p>Your car is covered and the car park is closed.</p>
            </li>
            <li><span class="glyphicon glyphicon-ok"></span><b>24/7</b>
                <p>We are open 24 hours a day for 365 days. </p>
            </li>
            <li><span class="glyphicon glyphicon-ok"></span><b>Security</b>
                <p>Your car will be in the VIP parking garage, controlled by ATN security. </p>
            </li>
            <li><span class="glyphicon glyphicon-ok"></span><b>Available price</b>
                <p>The price is calculated only for parking for an entire hour. </p>
            </li>

        </ul>
    </div>
</div>
</body>

</html>