<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="metadata.jsp"/>

    <title>Sign In</title>

</head>
<body>
<jsp:include page="header.jsp"/>
<div class="container ">

    <form id="loginForm" action="/j_spring_security_check" method='POST'>
        <div class="row">
            <div class="form-group col-sm-4 col-md-offset-4">
                <label for="login">Login:</label>
                <input type="text" class="form-control" id="login" placeholder="Login" name="username">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-sm-4 col-md-offset-4">
                <label for="password">Password:</label>
                <input type="password" class="form-control" id="password" placeholder="Password" name="password">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-sm-4 col-md-offset-4" style="text-align:right">
                <button type="submit" class="btn" id="login-button">Sign in</button>
            </div>
        </div>
        <div id="bad-credentials" style="text-align:center">
        </div>
    </form>

</div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        if (window.location.href.indexOf("error=true") > -1) {
            $("#bad-credentials").append($("<p>Bad credentials</p>"))
            setTimeout(hideError, 5000)
        }


        var hideError = function () {
            $("#bad-credentials").empty();

        };
    });
</script>


</html>