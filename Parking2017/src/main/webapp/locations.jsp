<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="metadata.jsp"/>

    <title>Locations</title>

</head>
<body>
<jsp:include page="header.jsp"/>
</br>
</br>
<div class="container ">

    <div class="row" id="locations"></div>
</div>
</body>
<script>
    $(document).ready(function () {
        var path = ""

        $.ajax(
            {
                type: "GET",
                url: path + "/rest/locations",
                accept: "application/json",
                success: function (locations) {
                    for (var i = 0; i < locations.length; i++) {
                        console.log(locations[i])
                        $("#locations").append(renderTemplate("<div class''{{address}", locations[i]))

                        $("#locations").append($("<div class=\"col-sm-4\">" +
                            "<h1>" + locations[i].address + "</h1>" +
                            "<p>Parking capacity:" + locations[i].numberSlots + "</p>" +
                            "<p>Occupied Slots:" + locations[i].numberOccupiedSlots + "</p>" +
                            "<p>Free Slots:" + (locations[i].numberSlots - locations[i].numberOccupiedSlots) + "</p>" +
                            "<p>Price for hour:" + locations[i].priceForHour + "</p>" +
                            "</div>"));
                    }
                }
            });
    })


</script>
</html>
