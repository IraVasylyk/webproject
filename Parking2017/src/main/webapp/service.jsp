<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 01.12.2017
  Time: 18:21
  To change this template use File | Settings | File Templates.
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="metadata.jsp"/>
    <title>Parking management</title>
</head>
<body>

<jsp:include page="header.jsp"/>
</br>
</br>
<div class="container page">
</div>


<%--receipt model--%>
<div class="modal fade" id="pick-up-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content bord">
            <div class="modal-header header">
                <h2>Receipt</h2>
            </div>
            <div class="modal-body">
                <h4>Reservation code: <span class="reservation-code"></span></h4>
                <div class="row">
                    <div class="col-sm-6">
                        <h4>Model: <span class="model"></span></h4>
                    </div>
                    <div class="col-sm-6">
                        <h4>Number Plate:<span class="numberPlate"></span></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <h4>Location: <span class="location"></span></h4>
                    </div>
                    <div class="col-sm-6">
                        <h4>Price for hour: <span class="price-for-hour"></span></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <h4>Time reserve on: <span class="time-reserve-on"></span></h4>
                    </div>
                    <div class="col-sm-6">
                        <h4>Time reserve until:<span class="time-reserve-until"></span></h4>
                    </div>
                </div>
                <h2 style="text-align:right"> Total price: <span class="price"></span></h2>
            </div>
            <div class="modal-footer footer">
                <button type="button" class='btn' id="pay">Pay</button>
            </div>
        </div>
    </div>
</div>

<%--owners model--%>
<div class="modal fade" id="owners-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content bord">
            <div class="modal-header header">
                <h3>Number Plate: <span class="numberPlate"></span></h3>
            </div>
            <div class="modal-body">
                <form id="customers">
                </form>

            </div>
            <div class="modal-footer footer">
                <button type="button" class='btn' id="addOwners">Add owners</button>
            </div>
        </div>
    </div>
</div>


<%--park model--%>
<div class="modal fade" id="park-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content bord">
            <div class="modal-header header">
                <h3>Number Plate: <span class="numberPlate"></span></h3>
                <h3>Choose location:</h3>
            </div>
            <div class="modal-body">
                <form id="park-locations">
                </form>
            </div>
            <div class="modal-footer footer">
                <button type="button" class='btn' id="park">Park</button>
            </div>
        </div>
    </div>
</div>


</div>
<%--add vehicle--%>
<div class="modal fade" id="add-vehicle-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header header">
                <h3>Add new vehicle:</h3>

            </div>
            <div class="modal-body">
                <form id="addVehicleForm">
                    <div class="row">
                        <div class="form-group col-sm-4 col-md-offset-4">
                            <label for="numberPlate">Number plate:</label>
                            <input type="text" class="form-control" id="numberPlate" placeholder="Number plate">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-4 col-md-offset-4">
                            <label for="model">Model:</label>
                            <input type="text" class="form-control" id="model" placeholder="Model">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer footer">
                <button type="button" class='btn' id="addVehicle">Add Vehicle</button>
            </div>
        </div>
    </div>
</div>


</div>
</body>
<script>
    $(document).ready(function () {

        var path = ""
        templateWelcomePage()
        $(document).on("click", "#brand-ref", function () {
            templateWelcomePage()
        });

        $(document).on("click", "#beClient-ref", function () {
            templateBeClientPage()
        });

        $(document).on("click", "#location-ref", function () {
            $.ajax(
                {
                    type: "GET",
                    url: path + "/rest/locations",
                    accept: "application/json",
                    success: function (locations) {
                        $('body').find(".page").empty()
                        $('body').find(".page").append($(templateLocationPage))
                        for (var i = 0; i < locations.length; i++) {
                            //template
                            $("#locations").append($(renderObjectTemplate(
                                "<div class=\'col-sm-4\'><h1>{{address}}</h1><p>Parking capacity:{{numberSlots}}</p>" +
                                "<p>Occupied Slots:{{numberOccupiedSlots}}</p><p>Free Slots:{{freeSlots}}</p>" +
                                "<p>Price for hour:{{priceForHour}}</p></div>", locations[i])));

                        }
                    },
                    error: function (result) {
                        console.log(result.responseText)
                        var parse = JSON.parse(result.responseText);
                        alert(parse.message + ", url:" + parse.url)
                    }
                });
        });

// get all vehicle by customer
        $(document).on("click", "#service-ref", function () {
            $.ajax(
                {
                    type: "GET",
                    url: path + "/rest/customer/vehicles",
                    accept: "application/json",
                    success: function (vehicles) {
                        $('body').find(".page").empty()
                        $('body').find(".page").append($(templateVehiclesPage))
                        for (var i = 0; i < vehicles.length; i++) {
                            console.log(vehicles[i])
//template
                            var div;
                            if (vehicles[i].parked == true) {
                                div = renderObjectTemplate("<div class=\'vehicle\' id=\'{{numberPlate}}\'><h1>Model: <span class=\'model\'>{{model}}<span></h1>" +
                                    "<h3>Number plate: <span class=\'numberPlate\'>{{numberPlate}}</span></h3><p class=\'reservation-code\'> Current reservation code:{{currentReservationCode}}</p>" +
                                    "<div class=\'row\'><div class=\'col-sm-8\'></div><div class=\'col-sm-4\'><button  type=\'button\' class=\' btn owners\' number-vehicle=\'{{numberPlate}}\'>Owners</button>" +
                                    "<button  type=\'button\' class=\' btn addOwners\' number-vehicle=\'{{numberPlate}}\'>Add owners</button>" +
                                    "<button  type=\'button\' class=\' btn pickUp\' number-vehicle=\'{{numberPlate}}\' id-reserve=\'{{currentReservationCode}}\'>Pick Up</button></div></div>", vehicles[i])
                            } else {
                                div = renderObjectTemplate("<div class=\'vehicle\' id=\'{{numberPlate}}\'><h1>Model: <span class=\'model\'>{{model}}<span></h1>" +
                                    "<h3>Number plate: <span class=\'numberPlate\'>{{numberPlate}}</span></h3><p class=\'reservation-code\'></p>" +
                                    "<div class=\'row\'><div class=\'col-sm-8\'></div><div class=\'col-sm-4\'><button  type=\'button\' class=\' btn owners\' number-vehicle=\'{{numberPlate}}\'>Owners</button>" +
                                    "<button  type=\'button\' class=\' btn addOwners\' number-vehicle=\'{{numberPlate}}\'>Add owners</button>" +
                                    "<button  type=\'button\' class=\' btn park\' number-vehicle=\'{{numberPlate}}\'>Park</button></div></div>", vehicles[i])
                            }

                            $('body').find(".page").find("#vehicles").append($(div));
                        }
                    },
                    error: function (result) {
                        console.log(result.responseText)
                        var parse = JSON.parse(result.responseText);
                        alert(parse.message + ", url:" + parse.url)
                    }
                })
        });


//open  pick up model
        $(document).on("click", ".pickUp", function () {
            var idReservation = $(this).attr("id-reserve");
            $.ajax(
                {
                    type: "GET",
                    url: path + "/rest/reservation/price",
                    accept: "application/json",
                    data: {id_reservation: idReservation},
                    success: function (reservation) {
                        $('#pick-up-modal').find(".model").text(reservation.model);
                        $('#pick-up-modal').find(".numberPlate").text(reservation.number);
                        $('#pick-up-modal').find(".reservation-code").text(reservation.idReservation);
                        $('#pick-up-modal').find(".price").text(reservation.price);
                        $('#pick-up-modal').find(".price-for-hour").text(reservation.priceForHour);
                        $('#pick-up-modal').find(".time-reserve-on").text(getDate(reservation.timeReserveOn));
                        $('#pick-up-modal').find(".time-reserve-until").text(getDate(reservation.timeReserveUntil));
                        $('#pick-up-modal').find(".location").text(reservation.address);
                        $('#pick-up-modal').modal('toggle');
                    },
                    error: function (result) {
                        console.log(result.responseText)
                        var parse = JSON.parse(result.responseText);
                        alert(parse.message + ", url:" + parse.url)
                    }
                })
        });


//pay receipt and pickUp

        $(document).on("click", "#pay", function () {
            var idReservation = $('#pick-up-modal').find(".reservation-code").text();
            var numberVehicle = $('#pick-up-modal').find(".numberPlate").text();


            $.ajax(
                {
                    type: "POST",
                    url: path + "/rest/vehicle/pickUp",

                    data: {id_reservation: idReservation},
                    success: function (data) {

                        $('#pick-up-modal').find(".model").text("");
                        $('#pick-up-modal').find(".numberPlate").text("");
                        $('#pick-up-modal').find(".reservation-code").text("");
                        $('#pick-up-modal').find(".price").text("");
                        $('#pick-up-modal').find(".time-reserve-on").text("");
                        $('#pick-up-modal').find(".time-reserve-until").text("");
                        $('#pick-up-modal').find(".location").text("");
                        $('#pick-up-modal').modal('toggle');
                        var numberVehicleObject = {
                            numberVehicle: numberVehicle
                        }

                        $('body').find(".page").find("#vehicles").find('#' + numberVehicle).find('.reservation-code').empty();
                        $('body').find(".page").find("#vehicles").find('#' + numberVehicle).find('.col-sm-4').empty();
                        //template
                        $('body').find(".page").find("#vehicles").find('#' + numberVehicle).find('.col-sm-4').append($(
                            renderObjectTemplate(
                                "<button  type=\'button\' class=\' btn owners\' number-vehicle=\'{{numberVehicle}}\'>Owners</button>" +
                                "<button  type=\'button\' class=\' btn addOwners\' number-vehicle=\'{{numberVehicle}}\'>Add owners</button>" +
                                "<button  type=\'button\' class=\' btn park\' number-vehicle=\'{{numberVehicle}}\'>Park</button>", numberVehicleObject)))
                    },
                    error: function (result) {
                        console.log(result.responseText)
                        var parse = JSON.parse(result.responseText);
                        alert(parse.message + ", url:" + parse.url)
                    }
                })
        });

// open model with possible owners
        $(document).on("click", ".addOwners", function () {
            var numberPlate = $(this).attr("number-vehicle");


            $.ajax(
                {
                    type: "GET",
                    url: path + "/rest/vehicle/owners/possible",
                    accept: "application/json",
                    data: {numberPlate: numberPlate},
                    success: function (customers) {
                        $("#customers").empty();
                        $("#owners-modal").find(".modal-footer").empty();
                        if (customers.length != 0) {
                            //template
                            for (var i = 0; i < customers.length; i++) {

                                $("#customers").append($(
                                    renderObjectTemplate("  <div class=\'checkbox\'><h4><label><input type=\'checkbox\' class=\'owner\' value=\'{{login}}\'>" +
                                        "login: {{login}}, name: {{firstName}} {{secondName}}</label></h4></div>", customers[i])));
                            }
                            $("#owners-modal").find(".modal-footer").append($(" <button  type=\"button\" class=\'btn\' id=\"addOwners\" >Add owners</button>"))

                        } else {
                            $("#customers").append($("<h4>All customers own the vehicle.</h4>"))
                            $("#owners-modal").find(".modal-footer").append($(" <button  type=\"button\" class=\'btn\' id=\"close-owners-modal\" >Close</button>"))

                        }


                        $('#owners-modal').find(".numberPlate").text(numberPlate);
                        $('#owners-modal').modal('toggle');
                    },
                    error: function (result) {
                        console.log(result.responseText)
                        var parse = JSON.parse(result.responseText);
                        alert(parse.message + ", url:" + parse.url)
                    }
                })
        });


// add selected owners to specific vehicle
        $(document).on("click", "#addOwners", function () {
            var numberPlate = $('#owners-modal').find(".numberPlate").text();
            var newOwners = []
            $("#customers").find('.owner:checkbox:checked').each(function () {
                newOwners[newOwners.length] = $(this).val();
            })
            console.log(JSON.stringify(newOwners))
            $.ajax(
                {
                    type: "POST",
                    url: path + "/rest/" + numberPlate + "/owners/add",
                    contentType: "application/json",
                    data: JSON.stringify(newOwners),
                    success: function (result) {

                        $("#customers").empty();
                        newOwners = [];
                        $('#owners-modal').modal('toggle');
                        $("#owners-modal").find(".modal-footer").empty();
                    },
                    error: function (result) {
                        console.log(result.responseText)
                        var parse = JSON.parse(result.responseText);
                        alert(parse.message + ", url:" + parse.url)
                    }
                })
        });


        //open modal with current owners

        $(document).on("click", ".owners", function () {
            var numberPlate = $(this).attr("number-vehicle");


            $.ajax(
                {
                    type: "GET",
                    url: path + "/rest/vehicle/owners",
                    accept: "application/json",
                    data: {numberPlate: numberPlate},
                    success: function (customers) {
                        $("#customers").empty();
                        if (customers.length != 0) {
                            $("#customers").append($("<ul></ul>"));
                            for (var i = 0; i < customers.length; i++) {
                                console.log(customers[i])
//template
                                $("#customers").find("ul").append($(
                                    renderObjectTemplate("<li ><h4>login: {{login}}, name: {{firstName}} {{secondName}}</h4></li>", customers[i])));
                            }
                        } else {
                            $("#customers").append($("<h4>No customers own the vehicle.</h4>"))
                        }

                        $("#owners-modal").find(".modal-footer").empty();
                        $("#owners-modal").find(".modal-footer").append($(" <button  type=\'button\' class=\'btn\' id=\'close-owners-modal\' >Close</button>"))


                        $('#owners-modal').find(".numberPlate").text(numberPlate);
                        $('#owners-modal').modal('toggle');
                    },
                    error: function (result) {
                        console.log(result.responseText)
                        var parse = JSON.parse(result.responseText);
                        alert(parse.message + ", url:" + parse.url)
                    }
                })
        });


        $(document).on("click", "#close-owners-modal", function () {
            $('#owners-modal').modal('toggle');
            $("#owners-modal").find(".modal-footer").empty();
            $("#owners-modal").find("#customers").empty();
            $("#owners-modal").find(".numberPlate").text("");
        })


// open park model
        $(document).on("click", ".park", function () {
            var numberPlate = $(this).attr("number-vehicle");
            $("#park-locations").empty();
            $.ajax(
                {
                    type: "GET",
                    url: path + "/rest/locations",
                    accept: "application/json",
                    success: function (locations) {
                        for (var i = 0; i < locations.length; i++) {
                            console.log(locations[i])
                            var radiobutton;
                            if (locations[i].freeSlots == 0) {
                                radiobutton = renderObjectTemplate("<div class=\'radio\'><h4><label><input type=\'radio\' name=\'optradio\' class=\'location\' disabled " +
                                    "value=\'{{address}}\'>{{address}},  free-slots: {{freeSlots}},  price for hour: {{priceForHour}}</label></h4></div>", locations[i])
                            } else {
                                radiobutton = renderObjectTemplate("<div class=\'radio\'><h4><label><input type=\'radio\' name=\'optradio\' class=\'location\'" +
                                    "value=\'{{address}}\'>{{address}},  free-slots: {{freeSlots}},  price for hour: {{priceForHour}}</label></h4></div>", locations[i])
                            }
                            $("#park-locations").append($(radiobutton));

                        }
                        $('#park-modal').find(".numberPlate").text(numberPlate);
                        $('#park-modal').modal('toggle');
                    },
                    error: function (result) {
                        console.log(result.responseText)
                        var parse = JSON.parse(result.responseText);
                        alert(parse.message + ", url:" + parse.url)
                    }


                })
        });


// park vehicle
        $(document).on("click", "#park", function () {

            var number = $('#park-modal').find(".numberPlate").text();

            var address = $("#park-locations").find("input[type='radio']:checked").val()
            var reservationDto = {
                address: address,
                number: number
            }
            console.log(JSON.stringify(reservationDto))
            $.ajax(
                {
                    type: "POST",
                    url: path + "/rest/vehicle/park",
                    contentType: "application/json",
                    data: JSON.stringify(reservationDto),
                    success: function (idReservation) {

                        $('#park-modal').find(".numberPlate").text("");
                        $('#park-modal').find("#park-location").empty();
                        $('#park-modal').modal('toggle');
                        var numberVehicleObject = {
                            number: number,
                            idReservation: idReservation
                        }

                        $('body').find(".page").find("#vehicles").find('#' + number).find('.reservation-code').text("Current reservation code: " + idReservation);
                        $('body').find(".page").find("#vehicles").find('#' + number).find('.col-sm-4').empty();
                        $('body').find(".page").find("#vehicles").find('#' + number).find('.col-sm-4').append($(
                            //template
                            renderObjectTemplate("<button  type=\'button\' class=\' btn owners\' number-vehicle=\'{{number}}\'>Owners</button>" +
                                "<button  type=\'button\' class=\' btn addOwners\' number-vehicle=\'{{number}}\'>Add owners</button>" +
                                "<button  type=\'button\' class=\' btn pickUp\' number-vehicle=\'{{number}}\' id-reserve=\'{{idReservation}}\'>Pick Up</button>", numberVehicleObject)))
                    },
                    error: function (result) {
                        console.log(result.responseText)
                        var parse = JSON.parse(result.responseText);
                        alert(parse.message + ", url:" + parse.url)
                    }
                })
        });


        $(document).on("click", "#openAddVehicleForm", function () {
            addVehicleForm
            $("#addVehicleForm").find("#numberPlate").val("");
            $("#addVehicleForm").find("#model").val("");
            $('#add-vehicle-modal').modal('toggle');

        });

        $(document).on("click", "#addVehicle", function () {

            var number = $('#add-vehicle-modal').find("#numberPlate").val();

            var model = $("#add-vehicle-modal").find("#model").val()
            var vehicle = {
                model: model,
                numberPlate: number
            }
            console.log(JSON.stringify(vehicle))
            $.ajax(
                {
                    type: "POST",
                    url: path + "/rest/customer/vehicle/add",
                    contentType: "application/json",
                    data: JSON.stringify(vehicle),
                    success: function (result) {


                        $("#addVehicleForm").find("#numberPlate").val("");
                        $("#addVehicleForm").find("#model").val("");
                        $('#add-vehicle-modal').modal('toggle');
//template
                        var div = div = renderObjectTemplate("<div class=\'vehicle\' id=\'{{numberPlate}}\'><h1>Model: <span class=\'model\'>{{model}}<span></h1>" +
                            "<h3>Number plate: <span class=\'numberPlate\'>{{numberPlate}}</span></h3><p class=\'reservation-code\'></p>" +
                            "<div class=\'row\'><div class=\'col-sm-8\'></div><div class=\'col-sm-4\'><button  type=\'button\' class=\' btn owners\' number-vehicle=\'{{numberPlate}}\'>Owners</button>" +
                            "<button  type=\'button\' class=\' btn addOwners\' number-vehicle=\'{{numberPlate}}\'>Add owners</button>" +
                            "<button  type=\'button\' class=\' btn park\' number-vehicle=\'{{numberPlate}}\'>Park</button></div></div>", vehicle)

                        $('body').find(".page").find("#vehicles").append($(div));
                    },
                    error: function (result) {
                        console.log(result.responseText)
                        var parse = JSON.parse(result.responseText);
                        alert(parse.message + ", url:" + parse.url)
                    }
                })
        });

//be client functional

        $(document).on("click", "#registration", function () {
            var hasErrors = false;

            if ($('#beClient').find("#password").val() == "") {
                $('#beClient').find(".validation-error.password").text("The password can not be empty.")
                hasErrors = true;
            }
            if ($('#beClient').find("#login").val() == "") {
                $('#beClient').find(".validation-error.login").text("The login can not be empty.")
                hasErrors = true;
            }
            if ($('#beClient').find("#firstName").val() == "") {
                $('#beClient').find(".validation-error.firstName").text("The name can not be empty.")
                hasErrors = true;
            }
            if ($('#beClient').find("#secondName").val() == "") {
                $('#beClient').find(".validation-error.secondName").text("The surname can not be empty.")
                hasErrors = true;
            }
            if ($('#beClient').find("#phone").val() == "") {
                $('#beClient').find(".validation-error.phone").text("The phone can not be empty.")
                hasErrors = true;
            }
            if ($('#beClient').find("#confPassword").val() == "") {
                $('#beClient').find(".validation-error.confPassword").text("The confirmation password can not be empty.")
                hasErrors = true;
            }


            if ($('#beClient').find("#password").val() != $('#beClient').find("#confPassword").val()) {
                $('#beClient').find(".validation-error.password").text("The password and confirmation password do not match.");
                $('#beClient').find(".validation-error.confPassword").text("The password and confirmation password do not match.");
                hasErrors = true;

            }
            if (hasErrors) {
                setTimeout(hideError, 5000);
                return;
            }


            var customer = {
                login: $('#beClient').find("#login").val(),
                password: $('#beClient').find("#password").val(),
                firstName: $('#beClient').find("#firstName").val(),
                secondName: $('#beClient').find("#secondName").val(),
                phone: $('#beClient').find("#phone").val()
            }
            console.log(JSON.stringify(customer))
            $.ajax(
                {
                    type: "POST",
                    url: path + "/rest/customer/signUp",
                    contentType: "application/json",
                    data: JSON.stringify(customer),
                    success: function (result) {
                        alert("Congrats! You are our client now!")
                        window.location.href = path + "/login";
                    },
                    error: function (result) {
                        console.log(result.responseText)
                        let parse = JSON.parse(result.responseText);
                        for (var i = 0; i < parse.length; i++) {
                            console.log(parse[i])
                            $('#beClient').find(".validation-error." + parse[i].field).text(parse[i].defaultMessage);
                            setTimeout(hideError, 5000);
                        }
                    }
                })
        });


        function templateWelcomePage() {
            var welcomePage = "<img  class=\'welcome-image\' src=\'${pageContext.request.contextPath}/resources/img/parkingGreen.png'>" +
                " <div id=\'benefits\'>" +
                "<h3>Why you should be our client?</h3>" +
                "<ul style=\'list-style-type:none\'>" +
                "<li><span class=\'glyphicon glyphicon-ok\'></span><b>Taked care of the camera</b>" +
                "<p>Your car is equipped with a VIP-park 24 hours a day, 7 days a week with camera monitoring.</p>" +
                "</li>" +
                "<li><span class=\'glyphicon glyphicon-ok\'></span><b>Covered parking lot</b>" +
                "<p>Your car is covered and the car park is closed.</p>" +
                "</li>" +
                "<li><span class=\'glyphicon glyphicon-ok\'></span><b>24/7</b>" +
                " <p>We are open 24 hours a day for 365 days.            </p>" +
                "</li>" +
                "<li><span class=\'glyphicon glyphicon-ok\'></span><b>Security</b>" +
                "<p>Your car will be in the VIP parking garage, controlled by ATN security. </p>" +
                " </li>" +
                "<li><span class=\'glyphicon glyphicon-ok\'></span><b>Available price</b>" +
                "<p>The price is calculated only for parking for an entire hour. </p>" +
                "</li>" +
                "</ul>" +
                "</div>"
            $('body').find('.page').empty();
            $('body').find('.page').append(welcomePage);
        }

        function templateBeClientPage() {
            var beClientPage = "<form id=\'beClient\' >" +
                "<div class=\'row\'>" +
                "<div class=\'form-group col-sm-4 col-md-offset-4\'>" +
                "<label for=\'login\'>Login:</label>" +
                "<input type=\'text\' class=\'form-control\' id=\'login\' placeholder=\'Login\'>" +
                "<span class=\'validation-error login\'></span>" +
                "</div>" +
                "</div>" +
                "<div class=\'row\'>" +
                "<div class=\'form-group col-sm-4 col-md-offset-4\'>" +
                "<label for=\'password\'>Password:</label>" +
                "<input type=\'password\' class=\'form-control\'id=\'password\' placeholder=\'Password\'>" +
                "<span class=\'validation-error password\'></span>" +

                "</div>" +
                "</div>" +
                "<div class=\'row\'>" +
                "<div class=\'form-group col-sm-4 col-md-offset-4 \'>" +
                "<label for=\'confPassword\'>Confirm password:</label>" +
                "<input type=\'password\' class=\'form-control\' id=\'confPassword\' placeholder=\'Password\'>" +
                "<span class=\'validation-error confPassword\'></span>" +

                "</div>" +
                "</div>" +

                "<div class=\'row\'>" +
                "<div class=\'form-group col-sm-4 col-md-offset-4 \'>" +
                "<label for=\'firstName\'>Name:</label>" +
                "<input type=\'text\' class=\'form-control\' id=\'firstName\' placeholder=\'Name\'>" +
                "<span class=\'validation-error firstName\'></span>" +

                "</div>" +

                "</div>" +
                "<div class=\'row\'>" +
                "<div class=\'form-group col-sm-4 col-md-offset-4\'>" +
                "<label for=\'secondName\'>Surname:</label>" +
                "<input type=\'text\' class=\'form-control\' id=\'secondName\' placeholder=\'Surname\'>" +
                "<span class=\'validation-error secondName\'></span>" +


                "</div>" +
                "</div>" +
                "<div class=\'row\'>" +
                "<div class=\'form-group col-sm-4 col-md-offset-4\'>" +
                "<label for=\'phone\'>Phone:</label>" +
                "<input type=\'text\' class=\'form-control\' id=\'phone\' placeholder=\'Phone\'>" +
                "<span class=\'validation-error phone\'></span>" +

                " </div>" +
                "</div>" +

                "<div class=\'row\'>" +
                "<div class=\'form-group col-sm-4 col-md-offset-4\'  style=\'text-align:right\'>" +
                "<button type=\'button\' class=\'btn\' id=\'registration\'>Sign up</button>" +

                "</div>" +
                "</div>" +
                "</form>"

            $('body').find('.page').empty();
            $('body').find('.page').append(beClientPage);
        }

        var templateVehiclesPage = "<img  class=\'welcome-image\' src=\'${pageContext.request.contextPath}/resources/img/parking1.jpg'>" +
            "<div class=\'vehicle-service\'>" +
            "<div class=\'row\'><div class=\'col-md-9\'></div><div class=\'col-md-3\'>" +
            "<button  type=\'button\' class=\'btn\'  id=\'openAddVehicleForm\'> Register new vehicle</button></div></div>" +
            "<div id=\'vehicles\'></div></div>";

        var templateLocationPage = "<div class=\'row\' id =\'locations\'></div>"

    })

    function getDate(timestamp) {
        var date = new Date(timestamp);
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        var formattedTime = hours + ':' + minutes + ':' + seconds;
        return formattedTime;
    }

    function renderObjectTemplate(template, model) {
        var indexOfStart = 0;
        var indexOfEnd = 0;
        indexOfStart = template.indexOf("{{");
        indexOfEnd = template.indexOf("}}");
        while (indexOfStart != -1) {
            var parameter = template.substring(indexOfStart + 2, indexOfEnd)
            template = template.replace("{{" + parameter + "}}", model[parameter])
            indexOfStart = template.indexOf("{{");
            indexOfEnd = template.indexOf("}}");
        }
        return template;
    }


</script>
</html>