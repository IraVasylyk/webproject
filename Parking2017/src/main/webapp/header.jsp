<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container header">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" id="brand-ref">Parking management</a>
        </div>


        <div class="collapse navbar-collapse" id="Navbar">
            <ul class="nav navbar-nav navbar-right" id="menu">

                <li><a id="location-ref">Locations</a></li>
                <c:if test="${pageContext.request.userPrincipal.name == null}">

                    <li><a id="beClient-ref">Be our client</a></li>
                    <li><a href="${pageContext.request.contextPath}/login">Sign in</a></li>

                </c:if>
                <c:if test="${pageContext.request.userPrincipal.name != null}">

                    <li><a id="service-ref">Park/PickUp</a></li>
                    <li><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false"><span
                            class="glyphicon glyphicon-user"></span> ${pageContext.request.userPrincipal.name}<span
                            class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="${pageContext.request.contextPath}/logout">Sign out</a></li>
                        </ul>
                    </li>
                </c:if>

                <li></li>
            </ul>
        </div>

    </div>

</nav>
