<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="metadata.jsp"/>

    <title>Registration</title>

</head>
<body>
<jsp:include page="header.jsp"/>
<div class="container ">

    <form id="beClient">
        <div class="row">
            <div class="form-group col-sm-4 col-md-offset-4">
                <label for="login">Login:</label>
                <input type="text" class="form-control" id="login" placeholder="Login">
                <span class="validation-error login"></span>
            </div>

        </div>
        <div class="row">
            <div class="form-group col-sm-4 col-md-offset-4">
                <label for="password">Password:</label>
                <input type="password" class="form-control" id="password" placeholder="Password">
                <span class="validation-error password"></span>

            </div>
        </div>
        <div class="row">
            <div class="form-group col-sm-4 col-md-offset-4 ">
                <label for="confPassword">Confirm password:</label>
                <input type="password" class="form-control" id="confPassword" placeholder="Password">
                <span class="validation-error confPassword"></span>

            </div>
        </div>

        <div class="row">
            <div class="form-group col-sm-4 col-md-offset-4 ">
                <label for="firstName">Name:</label>
                <input type="text" class="form-control" id="firstName" placeholder="Name">
                <span class="validation-error firstName"></span>

            </div>

        </div>
        <div class="row">
            <div class="form-group col-sm-4 col-md-offset-4">
                <label for="secondName">Surname:</label>
                <input type="text" class="form-control" id="secondName" placeholder="Surname">
                <span class="validation-error secondName"></span>


            </div>
        </div>
        <div class="row">
            <div class="form-group col-sm-4 col-md-offset-4">
                <label for="phone">Phone:</label>
                <input type="text" class="form-control" id="phone" placeholder="Phone">
                <span class="validation-error phone"></span>

            </div>
        </div>

        <div class="row">
            <div class="form-group col-sm-4 col-md-offset-4" style="text-align:right">
                <button type="button" class="btn" id="registration">Sign up</button>

            </div>
        </div>


    </form>
</div>


</body>
<script>
    $(document).ready(function () {

        var path = ""

        $(document).on("click", "#registration", function () {
            var hasErrors = false;

            if ($('#beClient').find("#password").val() == "") {
                $('#beClient').find(".validation-error.password").text("The password can not be empty.")
                hasErrors = true;
            }
            if ($('#beClient').find("#login").val() == "") {
                $('#beClient').find(".validation-error.login").text("The login can not be empty.")
                hasErrors = true;
            }
            if ($('#beClient').find("#firstName").val() == "") {
                $('#beClient').find(".validation-error.firstName").text("The name can not be empty.")
                hasErrors = true;
            }
            if ($('#beClient').find("#secondName").val() == "") {
                $('#beClient').find(".validation-error.secondName").text("The surname can not be empty.")
                hasErrors = true;
            }
            if ($('#beClient').find("#phone").val() == "") {
                $('#beClient').find(".validation-error.phone").text("The phone can not be empty.")
                hasErrors = true;
            }
            if ($('#beClient').find("#confPassword").val() == "") {
                $('#beClient').find(".validation-error.confPassword").text("The confirmation password can not be empty.")
                hasErrors = true;
            }


            if ($('#beClient').find("#password").val() != $('#beClient').find("#confPassword").val()) {
                $('#beClient').find(".validation-error.password").text("The password and confirmation password do not match.");
                $('#beClient').find(".validation-error.confPassword").text("The password and confirmation password do not match.");
                hasErrors = true;

            }
            if (hasErrors) {
                setTimeout(hideError, 5000);
                return;
            }


            var customer = {
                login: $('#beClient').find("#login").val(),
                password: $('#beClient').find("#password").val(),
                firstName: $('#beClient').find("#firstName").val(),
                secondName: $('#beClient').find("#secondName").val(),
                phone: $('#beClient').find("#phone").val()
            }
            console.log(JSON.stringify(customer))
            $.ajax(
                {
                    type: "POST",
                    url: path + "/rest/customer/signUp",
                    contentType: "application/json",
                    data: JSON.stringify(customer),
                    success: function (result) {
                        alert("Congrats! You are our client now!")
                        window.location.href = path + "/login";
                    },
                    error: function (result) {
                        console.log(result.responseText)
                        let parse = JSON.parse(result.responseText);
                        for (var i = 0; i < parse.length; i++) {
                            console.log(parse[i])
                            $('#beClient').find(".validation-error." + parse[i].field).text(parse[i].defaultMessage);
                            setTimeout(hideError, 5000);
                        }
                    }
                })
        });


    })
    var hideError = function () {
        $(".validation-error").text("");

    };
</script>
</html>